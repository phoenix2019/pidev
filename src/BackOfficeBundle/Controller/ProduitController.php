<?php
/**
 * Created by PhpStorm.
 * User: omark
 * Date: 26/02/2019
 * Time: 22:25
 */

namespace BackOfficeBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProduitController extends Controller
{

    public function AfficherAllProduitAction()
    {
        $em=$this->getDoctrine()->getManager();

        $produit=$em->getRepository('AppBundle:produit')->findAll();

        return $this->render('@BackOffice/Produit/AfficheAll.html.twig',array('produits'=>$produit));
    }
}