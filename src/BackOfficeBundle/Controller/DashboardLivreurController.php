<?php

namespace BackOfficeBundle\Controller;

use AppBundle\Entity\produit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\livraison;
use AppBundle\Repository\livraisonRepository;

use AppBundle\Form\livraisonType;

class DashboardLivreurController extends Controller
{

    public function DashboardLivreurAction()
    {
        return $this->render('@BackOffice/DashboardLivreur/DashboardLivreur.html.twig', array(
            // ...
        ));
    }

    public function ListeLivraisonAction()
    {
        $em=$this->getDoctrine();
        $livraison=$em->getRepository(livraison::class)->findAll();
        return $this->render('@BackOffice/DashboardLivreur/ListeLivraison.html.twig', array(
            "livraisons"=>$livraison
        ));
    }

    public function SelectDateAction(Request $request,$idlivraison)
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) ) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
        }
        $idLivreur=$user->getId();
       // $date3 = new \DateTime('now');
        $repository = $this->getDoctrine()->getRepository(livraison::class);
        $ok = $repository->find($idlivraison);
        $ok->setIdLivreur($idLivreur);
        $ok->setEtat("En cours");
        $em = $this->getDoctrine()->getManager();

        $em->flush();
        return $this->render('@BackOffice/DashboardLivreur/SelectDate.html.twig', array("livraison"=>$ok
            // ...
        ));
    }
    public function AccepterLivraisonAction(Request $request,$idlivraison){


        $repository = $this->getDoctrine()->getRepository(livraison::class);
        $ok = $repository->find($idlivraison);


        if ($request->isMethod('POST')) {


            $ok->setHeureLivraison(\DateTime::createFromFormat('H:i',$request->get('heurelivraison')));

            $date=new \DateTime($request->get('datelivraison'));
            $ok->setDateLivraison($date);


        $em = $this->getDoctrine()->getManager();

        $em->persist($ok);
        $em->flush();

        }
        return $this->render('@BackOffice/DashboardLivreur/DashboardLivreur.html.twig', array(
            // ...
        ));

    }



    public function MesLivraisonAction()
    {
        $em=$this->getDoctrine()->getManager();
        $livraison=$em->getRepository('AppBundle:livraison')->findBy(array('idLivreur'=>$this->getUser()));
        return $this->render('@BackOffice/DashboardLivreur/MesLivraison.html.twig', array("livraisons"=>$livraison
            // ...
        ));
    }






    public function UneLivraisonAction($idlivraison)
    {

        $livREPOS = $this->getDoctrine()->getRepository(livraison::class);
        $ok = $livREPOS->find($idlivraison);
        $idproduit=$ok->getIdProduit();
        $produit = $this->getDoctrine()->getRepository(produit::class)->find($idproduit);
        //dump($produit);dump($ok);die();
        $liv = $livREPOS->findBy(array('idlivraison'=>$idlivraison));
        $em=$this->getDoctrine()->getManager();
//        dump($liv);
//        die();
        return $this->render('@BackOffice/DashboardLivreur/UneLivraison.html.twig', array('livraisons'=>$liv,'produit'=>$produit
            // ...
        ));

    }

    public function RechercheAction(Request $request)
    {
        if ($request->isMethod('POST')) {


        $longitude_source_saisie=$request->get('longitude_source_saisie');
        $latitude_source_saisie=$request->get('latitude_source_saisie');
        $longitude_destination_saisie=$request->get('longitude_destination_saisie');
        $latitude_destination_saisie=$request->get('latitude_destination_saisie');

        $em = $this->getDoctrine()->getManager();
        $ok=$em->getRepository('AppBundle:livraison')->RechercheLivraison($longitude_source_saisie,$latitude_source_saisie,$longitude_destination_saisie,$latitude_destination_saisie);


            return $this->render('@BackOffice/DashboardLivreur/recherchelivraison.html.twig', array("livraisons"=>$ok
                // ...
            ));



        }
    }


    public function SimpleMapAction()
    {
        return $this->render('@BackOffice/DashboardLivreur/SimpleMap.html.twig', array(
            // ...
        ));
    }

    public function TerminerLivraisonAction($idlivraison){


        $repository = $this->getDoctrine()->getRepository(livraison::class);
        $ok = $repository->find($idlivraison);
        $ok->setEtat("Terminée");
        $em = $this->getDoctrine()->getManager();
         $em->persist($ok);
        $em->flush();
        return $this->render('@BackOffice/DashboardLivreur/DashboardLivreur.html.twig', array());

    }

}
