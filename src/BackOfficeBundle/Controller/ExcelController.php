<?php
/**
 * Created by PhpStorm.
 * User: omark
 * Date: 26/02/2019
 * Time: 00:33
 */

namespace BackOfficeBundle\Controller;


use AppBundle\Entity\produit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ExcelController extends Controller
{
    public function indexAction()
    {

        $em=$this->getDoctrine()->getManager();
        $produit=$em->getRepository(produit::class)->findAll();

        return $this->render('@BackOffice/Excel/excel.xlsx.twig',array('produits'=>$produit));
    }
}