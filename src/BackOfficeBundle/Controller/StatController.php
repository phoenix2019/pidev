<?php
/**
 * Created by PhpStorm.
 * User: omark
 * Date: 25/02/2019
 * Time: 22:52
 */

namespace BackOfficeBundle\Controller;


use AppBundle\Entity\produit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Ob\HighchartsBundle\Highcharts\Highchart;

class StatController extends Controller
{

    public function StatAction()
    {


        $em1=$this->getDoctrine()->getManager();
        $query1 = $em1->createQuery("SELECT COUNT(p.idProduit) as N FROM AppBundle:produit p where p.corbeilleProduit='non'");
        $resultat1=$query1->getResult();
        foreach ($resultat1 as $values) {
            $a = $values['N'];
        }
        $ob = new Highchart();
        $ob->chart->renderTo('piechart');
        $ob->title->text('Il y '.$a.' produits classés selon les opérations à faire');
        $ob->plotOptions->pie(array(
            'allowPointSelect'  => true,
            'cursor'    => 'pointer',
            'dataLabels'    => array('enabled' => false),
            'showInLegend'  => true
        ));

        $em=$this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT COUNT(p.idProduit) as NBR,p.operationProduit as OP  FROM AppBundle:produit p where p.corbeilleProduit=:bool group by p.operationProduit
        ');
        $query->setParameter('bool', 'non');
       $resultat = $query->getResult();

        $data = array();
        foreach ($resultat as $values) {
            $a = array($values['OP'], intval($values['NBR']));
            array_push($data, $a);
        }
        $ob->series(array(array('type' => 'pie','name' => 'Browser share', 'data' => $data)));

        return $this->render('@BackOffice/Stat/stat.html.twig', array(
            'chart' => $ob
        ));
    }
}