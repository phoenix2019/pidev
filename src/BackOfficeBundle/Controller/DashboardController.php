<?php

namespace BackOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DashboardController extends Controller
{
    /**
     * @Route("/Dashboard")
     */
    public function DashboardAction()
    {
        return $this->render('@BackOffice/Dashboard/dashboard.html.twig', array(
            // ...
        ));
    }

    public function AjouterLivreurAction()
    {
        return $this->render('@BackOffice/DashboardLivreur/AjouterLivreur.html.twig', array(
            // ...
        ));
    }

}
