<?php
/**
 * Created by PhpStorm.
 * User: omark
 * Date: 19/02/2019
 * Time: 22:37
 */

namespace BackOfficeBundle\Controller;


use AppBundle\Entity\produit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Reparation;
use Symfony\Component\HttpFoundation\Request;

class ReparationController extends Controller
{

    public function AfficherUserAction()
    {

        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) ) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
        }
        else{
            return $this->redirectToRoute('fos_user_security_login');
        }
        $id_user=$user->getId();
        $em=$this->getDoctrine();
        $reparation=$em->getRepository('AppBundle:Reparation')->findReparation($id_user);

        return $this->render('@BackOffice/Reparation/AfficherUserReparation.html.twig',array("reparations"=>$reparation));

    }

    public function AfficherAllAction(){

        $em=$this->getDoctrine();
        $reparation=$em->getRepository('AppBundle:Reparation')->findAllReparation();
        return $this->render('@BackOffice/Reparation/AfficherAllReparation.html.twig',array("reparations"=>$reparation));

    }

    public function AffecterAction($id){

        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) ) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
        }
        else{
            return $this->redirectToRoute('fos_user_security_login');
        }
        $id_user=$user->getId();

        $em=$this->getDoctrine()->getManager();
        $reparation = $em->getRepository(Reparation::class)->find($id);

            $reparation->setIdReparateur($id_user);
            $reparation->setEtatReparation("en reparation");

            $em->flush();

            //$this->addFlash('success','Modification du produit effectuée');


        return $this->redirectToRoute('AfficherReparation');

    }

    public function ModifierAction(Request $request, $id)
    {

        $em=$this->getDoctrine()->getManager();
        $reparation = $em->getRepository(Reparation::class)->find($id);

        if ($request->isMethod('POST')) {


            $reparation->setPrixReparation($request->get('Prix'));
            $reparation->setEtatReparation($request->get('Etat'));
            if($request->get('Etat')=="termine"){
                $today = new \DateTime("now");

                $reparation->setDateReparation($today);
            }
            else{
                $reparation->setDateReparation(null);
            }

            $em->flush();

          //  $this->addFlash('success','Modification du produit effectuée');
            return $this->redirectToRoute('AfficherReparation');
        }

        return $this->render('@BackOffice/Reparation/ModifierReparation.html.twig', array(
            'reparation' => $reparation
        ));
    }

    public function RefuserAction($id){

        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) ) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
        }
        else{
            return $this->redirectToRoute('fos_user_security_login');
        }
        $id_user=$user->getId();

        $em=$this->getDoctrine()->getManager();
        $reparation = $em->getRepository(Reparation::class)->find($id);

        $reparation->setIdReparateur(null);
        $reparation->setEtatReparation("en cours");
        $reparation->setPrixReparation(null);
        $reparation->setDateReparation(null);

        $em->flush();

        //$this->addFlash('success','Modification du produit effectuée');


        return $this->redirectToRoute('AfficherReparation');

    }



}