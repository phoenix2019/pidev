<?php

namespace FrontOfficeBundle\Controller;
use AppBundle\Entity\facture;
use AppBundle\Entity\produit;
use AppBundle\Entity\Reparation;
use AppBundle\Entity\User;
use AppBundle\Repository\factureRepository;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class FactureController extends Controller
{


    public function ListeFactureAction(){

        $em=$this->getDoctrine();
        $facture=$em->getRepository('AppBundle:facture')->findBy(array('idmembre'=>$this->getUser()));
        $em=$this->getDoctrine()->getManager();


//        foreach( $facture as $f){
//            $idproduit=$f->getIdproduit();
//            $reparation=$em->getRepository('AppBundle:Reparation')->rechercheidproduit($idproduit);
//            $idreparation=$reparation->getIdReparation();
//                $prixreparation=$reparation->getPrixReparation();
//                $f->setIdreparation($idreparation);
//                $f->setPrixreparation($prixreparation);
//
//            $em = $this->getDoctrine()->getManager();
//
//            $em->persist($f);
//
//
//            $em->flush();
//
//        }

        return $this->render('@FrontOffice/Livraison/ListeFacture.html.twig',array("facture"=>$facture));
    }
    public function UneFactureAction($idfacture)
    {

        $em=$this->getDoctrine();
        $facture=$em->getRepository('AppBundle:facture')->find($idfacture);
        //$factREPOS = $this->getDoctrine()->getRepository('AppBundle:facture');
        //$ok = $facture->find($idfacture);
        $idproduit=$facture->getIdproduit();
        $idmembre=$facture->getIdmembre();
        $produit = $this->getDoctrine()->getRepository(produit::class)->find($idproduit);
        $membre = $this->getDoctrine()->getRepository(User::class)->find($idmembre);
        //dump($produit);dump($ok);die();
        //dump($ok);
//         dump($facture);
//         dump($produit);
//         dump($membre);
//
//        die();
        //$fact = $factREPOS->findBy(array('idfacture'=>$idfacture));
        $em=$this->getDoctrine()->getManager();
//        dump($liv);
//        die();
         return $this->render('@FrontOffice/Livraison/UneFacture.html.twig', array('facture'=>$facture,'produit'=>$produit,'membre'=>$membre
            // ...
        ));



    }


    public function FacturePDFAction($idfacture)
    {
        $snappy=$this->get('knp_snappy.pdf');
        $filename="test_snappy";

        $em=$this->getDoctrine();
        $facture=$em->getRepository('AppBundle:facture')->find($idfacture);
        //$factREPOS = $this->getDoctrine()->getRepository('AppBundle:facture');
        //$ok = $facture->find($idfacture);
        $idproduit=$facture->getIdproduit();
        $idmembre=$facture->getIdmembre();
        $produit = $this->getDoctrine()->getRepository(produit::class)->find($idproduit);
        $membre = $this->getDoctrine()->getRepository(User::class)->find($idmembre);
        //dump($produit);dump($ok);die();
        //dump($ok);
//         dump($facture);
//         dump($produit);
//         dump($membre);
//
//        die();
        //$fact = $factREPOS->findBy(array('idfacture'=>$idfacture));
        $em=$this->getDoctrine()->getManager();
//        dump($liv);
//        die();
        $html= $this->render('@FrontOffice/Livraison/facturepdf.html.twig', array('facture'=>$facture,'produit'=>$produit,'membre'=>$membre
            // ...
        ));

        return new Response(

            $snappy->getOutputFromHtml($html),
            200,array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline;filename="'.$filename.'.pdf"'
            )
        );


    }


}
