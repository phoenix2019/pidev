<?php
/**
 * Created by PhpStorm.
 * User: omark
 * Date: 20/02/2019
 * Time: 16:24
 */

namespace FrontOfficeBundle\Controller;


use AppBundle\Entity\Reparation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ReparationController extends Controller
{

    public function AfficherMemberReparationAction()
    {

        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
        }
        $id_user = $user->getId();

        $em = $this->getDoctrine()->getManager();

        $reparation = $em->getRepository(reparation::class)->findUserReparation($id_user);

        return $this->render('@FrontOffice/Reparation/ViewMemberReparation.html.twig', array('reparations' => $reparation));


    }
}