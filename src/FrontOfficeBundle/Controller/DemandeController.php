<?php
/**
 * Created by PhpStorm.
 * User: omark
 * Date: 25/02/2019
 * Time: 10:02
 */

namespace FrontOfficeBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Demande;
class DemandeController extends Controller
{

    public function AfficherDemandeAction()
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) ) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
        }
        $id_user=$user->getId();

        $em=$this->getDoctrine()->getManager();

//        $demande=$em->getRepository(demande::class)->findBy(array('idUser'=>$id_user));

        $demande=$em->getRepository(demande::class)->findEchange($id_user);

        return $this->render('@FrontOffice/Demande/AffichageRequestDemande.html.twig',array('demandes'=>$demande));


    }
}