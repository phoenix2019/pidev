<?php

namespace FrontOfficeBundle\Controller;

use AppBundle\Entity\Demande;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Echange;
use Symfony\Component\HttpFoundation\Request;

class EchangeController extends Controller
{
    public function EnvoyerDemandeAction(Request $request,$id1,$id2,$id3){


        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) ) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
        }
        $id_user=$user->getId();

        $em=$this->getDoctrine()->getManager();

        if(!($em->getRepository(echange::class)->findAvailableEchange($id3))) {
            $echange=$em->getRepository(echange::class)->findoneBy(array ('idProduit1'=>$id3));
            $echange->setEtatEchange("demande");
            $echange->setidUser2($id1);
            $echange->setiDProduit2($id2);
            $em->flush();

            $demande=new Demande();
            $demande->setIdEchange($echange->getidEchange());
            $demande->setEtatDemande('en attente');
            $demande->setIdUser($id_user);
            $em1=$this->getDoctrine()->getManager();
            $em1->persist($demande);
            $em1->flush();

            return $this->redirectToRoute('AccueilMembers');
        }
        //$this->addFlash('success','Demande envoyé ');
        else{
            $this->get('session')->getFlashBag()->add(
                'error',
                "Il y a déjà une demande d'échange "
            );

            //return $this->generateUrl('ViewProduit',array('id'=>$id3));
            return $this->redirect($this->generateUrl('ViewProduit',array('id'=>$id3)));
        }

    }

    public function AfficherEchangeAction()
    {
        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) ) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
        }
        $id_user=$user->getId();

        $em=$this->getDoctrine()->getManager();
        $echange=$em->getRepository(echange::class)->findBy(array ('idUser1'=>$id_user));


        return $this->render('@FrontOffice/Echange/AfficheMemberEchange.html.twig',array('echanges'=>$echange));
    }

    public function AccepterDemandeAction($idEchange)
    {
        $em=$this->getDoctrine()->getManager();
        $echange=$em->getRepository(echange::class)->find($idEchange);
        $echange->setEtatEchange("accepte");
        $em->flush();

        $em1=$this->getDoctrine()->getManager();
        $demande=$em1->getRepository(demande::class)->findOneBy(array('idEchange'=>$idEchange));
        $demande->setEtatDemande("accepte");
        $em1->flush();

        return $this->redirectToRoute('AfficheMemberEchange');

    }

    public function RefuserDemandeAction($idEchange)
    {
        $em=$this->getDoctrine()->getManager();
        $echange=$em->getRepository(echange::class)->find($idEchange);
        $echange->setEtatEchange("refuse");
        $echange->setidUSer2(null);
        $echange->setidProduit2(null);
        $em->flush();

        $em1=$this->getDoctrine()->getManager();
        $demande=$em1->getRepository(demande::class)->findOneBy(array('idEchange'=>$idEchange));
        $demande->setEtatDemande("refuse");
        $em1->flush();

        return $this->redirectToRoute('AfficheMemberEchange');

    }

//    public function AfficherRequestEchangeAction()
//    {
//        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) ) {
//            $user = $this->container->get('security.token_storage')->getToken()->getUser();
//        }
//        $id_user=$user->getId();
//
//        $em=$this->getDoctrine()->getManager();
//
//        $echange=$em->getRepository(echange::class)->findRequest($id_user);
//
//
//        return $this->render('@FrontOffice/Echange/AffichageRequestEchange.html.twig',array('echanges',$echange));
//
//    }
}
