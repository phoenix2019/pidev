<?php

namespace FrontOfficeBundle\Controller;

use AppBundle\Entity\facture;
use AppBundle\Entity\livraison;
use AppBundle\Form\livraisonType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LivraisonController extends Controller
{

    public function AjouterLivraisonAction(Request $request,$idproduit)
    {

        if( $this->container->get( 'security.authorization_checker' )->isGranted( 'IS_AUTHENTICATED_FULLY' ) ) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
        }

        $livraisons = $this->getDoctrine()->getRepository(livraison::class)->findAll();
        $reparation=$this->getDoctrine()->getRepository('AppBundle:Reparation')->findOneBy(['idProduit'=>$idproduit]);
        if ($reparation!=null) {
            $idreparation = $reparation->getIdReparation();
            $prixreparation = $reparation->getPrixReparation();
        }

        $x=0;
        $y = 0;
        if ($livraisons!=null){
        foreach( $livraisons as $a){
            if( ($a->getDistancelivraison() !=null ) && ($a ->getPrixlivraison() != null) )
            {
                $x += $a->getDistancelivraison();
                $y += $a->getPrixlivraison();


            }

        }

        $z = $y/$x;}
        $idMembre=$user->getId();

        $lastlivraison = $this->getDoctrine()->getRepository(livraison::class)->findOneBy([],['idlivraison' => 'desc']);

        $lastid=$lastlivraison->getIdlivraison();


        $livraison =  new livraison();
        $facture = new facture();
        $livraison->setIdMembre($idMembre);
        $facture->setIdmembre($idMembre);
        $facture->setIdlivraison($lastid+1);
        if ($reparation!=null) {
            $facture->setIdreparation($idreparation);
            $facture->setPrixreparation($prixreparation);
        }
        $livraison->setDatepublication(new \DateTime("now"));


        $livraison->setIdProduit($idproduit);
        $facture->setIdproduit($idproduit);
        /**********************/


        $form = $this->createForm(livraisonType::class,$livraison);
        $form->handleRequest($request);


        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            $var=$_POST["appbundle_livraison"]["prixlivraison"];

            $facture->setPrixlivraison($var);
            if ($reparation!=null) {
                $facture->setTotalfacture($var+$prixreparation);}
            else {$facture->setTotalfacture($var);}
            //dump($_POST["appbundle_livraison"]["prixlivraison"]); dump($request);die();
            $em->persist($livraison);
            $em->persist($facture);

            $em->flush();
            return $this->render('@FrontOffice/Livraison/livraisonaajoute.html.twig',array());
        }
        return $this->render('@FrontOffice/Livraison/AjouterLivraison.html.twig',array('form'=>$form->createView(),'prix'=>$z));
    }




    public function AfficherLivraisonAction(){

        $em=$this->getDoctrine();
        $livraison=$em->getRepository('AppBundle:livraison')->findBy(array('idMembre'=>$this->getUser()));;


        return $this->render('@FrontOffice/Livraison/AfficherLivraison.html.twig',array("livraisons"=>$livraison));
    }





    public function SupprimerLivraisonAction(Request $request,$idlivraison){
        $em = $this->getDoctrine()->getManager();

        $ok=$em->getRepository(livraison::class)->find($idlivraison);
            $em = $this->getDoctrine()->getManager();
            $em->remove($ok);
            $em->flush();
        return $this->redirectToRoute('AfficherLivraison');

    }


    public function ModifierLivraisonAction(Request $request,$idlivraison){
        $repository = $this->getDoctrine()->getRepository(livraison::class);
        $livraison = $repository->find($idlivraison);
        $livraisons = $this->getDoctrine()->getRepository(livraison::class)->findAll();
        $x=0;
        $y = 0;
        if ($livraisons!=null){
            foreach( $livraisons as $a){
                if( ($a->getDistancelivraison() !=null ) && ($a ->getPrixlivraison() != null) )
                {
                    $x += $a->getDistancelivraison();
                    $y += $a->getPrixlivraison();


                }

            }

            $z = $y/$x;}

        $form = $this->createForm(livraisonType::class,$livraison);
        $form->handleRequest($request);


        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            $em->persist($livraison);

            $em->flush();
            return $this->render('@FrontOffice/Livraison/livraisonmodifie.html.twig',array('form'=>$form->createView()));
        }
        return $this->render('@FrontOffice/Livraison/ModifierLivraison.html.twig',array('form'=>$form->createView(),'prix'=>$z));


    }




}
