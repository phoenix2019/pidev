<?php

namespace FrontOfficeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class AccueilController extends Controller
{
    public function AccueilAction()
    {
        return $this->render('@FrontOffice/accueil.html.twig');
    }
}
