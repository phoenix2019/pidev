<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends Controller
{
    public function addAction()
    {
        return $this->render('AppBundle:Security:add.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/Roles")
     */
    public function redirectAction()
    {

        $authChecker = $this->container->get('security.authorization_checker');

        if($authChecker->isGranted('ROLE_REPARATEUR')){
            return $this->render('@BackOffice/Reparation/DashboardReparateur.html.twig');
        }else if ($authChecker->isGranted('ROLE_ADMIN')){
        return $this->render('@BackOffice/Dashboard/dashboard.html.twig');
        }
        else if ($authChecker->isGranted('ROLE_LIVREUR')){
        return $this->render('@BackOffice/DashboardLivreur/DashboardLivreur.html.twig');
        }
        else{
            return $this->render('@FrontOffice/accueil.html.twig');

        }

    }

}
