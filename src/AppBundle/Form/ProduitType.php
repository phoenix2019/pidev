<?php

namespace AppBundle\Form;


use AppBundle\Entity\produit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class ProduitType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder->add('NomProduit',null,['constraints' => new Length(['min' => 10])],array('label'=>'Le nom du produit','required'=>true,))
            ->add('DescriptionProduit',TextareaType::class,['constraints' => new Length(['min' => 10])],array('label'=>'La description du produit'))
            ->add('OperationProduit',ChoiceType::class,
                [
                    'label'=>'Operation à faire',
                    'placeholder'=>'choisir une opération',
                    'choices' =>[
                        'à echanger'=> 'echanger',
                        'à donner'=>'donner',
                        'à recycler'=>'recycler',
                        'à réparer'=>'reparer'

                    ]
                ]
            )
            ->add('ImageProduit',FileType::class,array('label'=>'Ajouter une image','data_class'=>null))
            ->add('save',SubmitType::class);

        
    }



}