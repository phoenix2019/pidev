<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class livraisonType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            //->add('heureLivraison',HiddenType::class)
            ->add('adresseLivraisonSource')
            ->add('longitudeSource',HiddenType::class)
            ->add('latitudeSource',HiddenType::class)
            ->add('adresseLivraisonDestination')
            ->add('longitudeDestination',HiddenType::class)
            ->add('latitudeDestination',HiddenType::class)

            ->add('idproduit',HiddenType::class)
          //  ->add('datelivraison',HiddenType::class)
            ->add('etat',HiddenType::class)
            ->add('prixlivraison')
            ->add('distancelivraison',HiddenType::class)
            ->add('Confirmer?',SubmitType::class);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\livraison'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_livraison';
    }


}
