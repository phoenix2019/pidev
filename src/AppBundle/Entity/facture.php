<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * facture
 *
 * @ORM\Table(name="facture")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\factureRepository")
 */
class facture
{
    /**
     * @var int
     *
     * @ORM\Column(name="idfacture", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idfacture;

    /**
     * @var int
     *
     * @ORM\Column(name="idmembre", type="integer", nullable=true)
     */
    private $idmembre;

    /**
     * @var int
     *
     * @ORM\Column(name="idproduit", type="integer", nullable=true)
     */
    private $idproduit;

    /**
     * @var int
     *
     * @ORM\Column(name="idlivraison", type="integer", nullable=true)
     */
    private $idlivraison;

    /**
     * @var int
     *
     * @ORM\Column(name="idreparation", type="integer", nullable=true)
     */
    private $idreparation;

    /**
     * @var float
     *
     * @ORM\Column(name="totalfacture", type="float", nullable=true)
     */
    private $totalfacture;
    /**
     * @var float
     *
     * @ORM\Column(name="prixlivraison", type="float", nullable=true)
     */
    private $prixlivraison;
    /**
     * @var float
     *
     * @ORM\Column(name="prixreparation", type="float", nullable=true)
     */
    private $prixreparation;

    /**
     * @var string
     *
     * @ORM\Column(name="etatfacture", type="string", length=255, nullable=true)
     */
    private $etatfacture ='Non reglée';

    /**
     * @return int
     */
    public function getIdfacture()
    {
        return $this->idfacture;
    }




    /**
     * Set idmembre
     *
     * @param integer $idmembre
     *
     * @return facture
     */
    public function setIdmembre($idmembre)
    {
        $this->idmembre = $idmembre;

        return $this;
    }

    /**
     * Get idmembre
     *
     * @return int
     */
    public function getIdmembre()
    {
        return $this->idmembre;
    }

    /**
     * Set idproduit
     *
     * @param integer $idproduit
     *
     * @return facture
     */
    public function setIdproduit($idproduit)
    {
        $this->idproduit = $idproduit;

        return $this;
    }

    /**
     * Get idproduit
     *
     * @return int
     */
    public function getIdproduit()
    {
        return $this->idproduit;
    }

    /**
     * Set idlivraison
     *
     * @param integer $idlivraison
     *
     * @return facture
     */
    public function setIdlivraison($idlivraison)
    {
        $this->idlivraison = $idlivraison;

        return $this;
    }

    /**
     * Get idlivraison
     *
     * @return int
     */
    public function getIdlivraison()
    {
        return $this->idlivraison;
    }

    /**
     * Set idreparation
     *
     * @param integer $idreparation
     *
     * @return facture
     */
    public function setIdreparation($idreparation)
    {
        $this->idreparation = $idreparation;

        return $this;
    }

    /**
     * Get idreparation
     *
     * @return int
     */
    public function getIdreparation()
    {
        return $this->idreparation;
    }


    /**
     * Set etatfacture
     *
     * @param string $etatfacture
     *
     * @return facture
     */
    public function setEtatfacture($etatfacture)
    {
        $this->etatfacture = $etatfacture;

        return $this;
    }

    /**
     * Get etatfacture
     *
     * @return string
     */
    public function getEtatfacture()
    {
        return $this->etatfacture;
    }

    /**
     * @return float
     */
    public function getTotalfacture()
    {
        return $this->totalfacture;
    }

    /**
     * @param float $totalfacture
     */
    public function setTotalfacture($totalfacture)
    {
        $this->totalfacture = $totalfacture;
    }

    /**
     * @return float
     */
    public function getPrixlivraison()
    {
        return $this->prixlivraison;
    }

    /**
     * @param float $prixlivraison
     */
    public function setPrixlivraison($prixlivraison)
    {
        $this->prixlivraison = $prixlivraison;
    }

    /**
     * @return float
     */
    public function getPrixreparation()
    {
        return $this->prixreparation;
    }

    /**
     * @param float $prixreparation
     */
    public function setPrixreparation($prixreparation)
    {
        $this->prixreparation = $prixreparation;
    }



}

