<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 12/02/2019
 * Time: 09:58
 */

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    protected $id;
    /**
     * @ORM\Column(type="string")
     */

    protected $nom;

    /**
     * @ORM\Column(type="string")
     */

    protected $prenom;

    /**
     * @ORM\Column(type="string")
     */

    protected $adresse;

    /**
     * @ORM\Column(type="string")
     */

    protected $telephone;

    /**
     * @ORM\Column(type="string",nullable=true)
     */

    protected $photoMembre;

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param mixed $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getPhotoMembre()
    {
        return $this->photoMembre;
    }

    /**
     * @param mixed $photoMembre
     */
    public function setPhotoMembre($photoMembre)
    {
        $this->photoMembre = $photoMembre;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    public function __construct()
    {
        parent::__construct();
        $this->setRoles(["ROLE_MEMBRE"]);
    }
}