<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * livraison
 *
 * @ORM\Table(name="livraison")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\livraisonRepository")
 */
class livraison
{
    /**
     * @var int
     *
     * @ORM\Column(name="idlivraison", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idlivraison;

    /**
     * @return int
     */
    public function getIdlivraison()
    {
        return $this->idlivraison;
    }

    /**
     * @param int $idlivraison
     */
    public function setIdlivraison($idlivraison)
    {
        $this->idlivraison = $idlivraison;
    }

    /**
     * @return int
     */
    public function getIdMembre()
    {
        return $this->idMembre;
    }

    /**
     * @param int $idMembre
     */
    public function setIdMembre($idMembre)
    {
        $this->idMembre = $idMembre;
    }


    /**
     * @var int
     *
     * @ORM\Column(name="IdMembre", type="integer")
     */
    private $idMembre;

    /**
     * @return int
     */
    public function getIdLivreur()
    {
        return $this->idLivreur;
    }

    /**
     * @param int $idLivreur
     */
    public function setIdLivreur($idLivreur)
    {
        $this->idLivreur = $idLivreur;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="IdLivreur", type="integer",nullable=true)
     */
    private $idLivreur;

    /**
     * @var int
     *
     * @ORM\Column(name="IdProduit", type="integer",nullable=true)
     */
    private $idProduit;

    /**
     * @return int
     */
    public function getIdProduit()
    {
        return $this->idProduit;
    }

    /**
     * @param int $idProduit
     */
    public function setIdProduit($idProduit)
    {
        $this->idProduit = $idProduit;
    }





    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heure_livraison", type="time",nullable=true)
     */
    private $heureLivraison;



    /**
     * @var string
     *
     * @ORM\Column(name="adresse_livraison_source", type="string", length=255)
     */
    private $adresseLivraisonSource;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude_source", type="string", length=255, nullable=true)
     */
    private $longitudeSource;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude_source", type="string", length=255, nullable=true)
     */
    private $latitudeSource;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_livraison_destination", type="string", length=255)
     */
    private $adresseLivraisonDestination;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude_destination", type="string", length=255, nullable=true)
     */
    private $longitudeDestination;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude_destination", type="string", length=255, nullable=true)
     */
    private $latitudeDestination;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datelivraison", type="datetime", nullable=true)
     */
    private $datelivraison;

    /**
     * @var float
     *
     * @ORM\Column(name="prixlivraison", type="float", nullable=true)
     */
    private $prixlivraison;

    /**
     * @var float
     *
     * @ORM\Column(name="distancelivraison", type="float", nullable=true)
     */
    private $distancelivraison;

    /**
     * @return float
     */
    public function getDistancelivraison()
    {
        return $this->distancelivraison;
    }

    /**
     * @param float $distancelivraison
     */
    public function setDistancelivraison($distancelivraison)
    {
        $this->distancelivraison = $distancelivraison;
    }

    /**
     * @return float
     */
    public function getPrixlivraison()
    {
        return $this->prixlivraison;
    }

    /**
     * @param float $prixlivraison
     */
    public function setPrixlivraison($prixlivraison)
    {
        $this->prixlivraison = $prixlivraison;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string", length=255, nullable=true)
     */
    private $etat='non affectée';
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_publication", type="datetime",options={"default" = "CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $datepublication;

    /**
     * @return \DateTime
     */
    public function getDatepublication()
    {
        return $this->datepublication;
    }

    /**
     * @param \DateTime $datepublication
     */
    public function setDatepublication($datepublication)
    {
        $this->datepublication = $datepublication;
    }







    /**
     * Set heureLivraison
     *
     * @param \DateTime $heureLivraison
     *
     * @return livraison
     */
    public function setHeureLivraison($heureLivraison)
    {
        $this->heureLivraison = $heureLivraison;

        return $this;
    }

    /**
     * Get heureLivraison
     *
     * @return \DateTime
     */
    public function getHeureLivraison()
    {
        return $this->heureLivraison;
    }



    /**
     * Set adresseLivraisonSource
     *
     * @param string $adresseLivraisonSource
     *
     * @return livraison
     */
    public function setAdresseLivraisonSource($adresseLivraisonSource)
    {
        $this->adresseLivraisonSource = $adresseLivraisonSource;

        return $this;
    }

    /**
     * Get adresseLivraisonSource
     *
     * @return string
     */
    public function getAdresseLivraisonSource()
    {
        return $this->adresseLivraisonSource;
    }

    /**
     * Set longitudeSource
     *
     * @param string $longitudeSource
     *
     * @return livraison
     */
    public function setLongitudeSource($longitudeSource)
    {
        $this->longitudeSource = $longitudeSource;

        return $this;
    }

    /**
     * Get longitudeSource
     *
     * @return string
     */
    public function getLongitudeSource()
    {
        return $this->longitudeSource;
    }

    /**
     * Set latitudeSource
     *
     * @param string $latitudeSource
     *
     * @return livraison
     */
    public function setLatitudeSource($latitudeSource)
    {
        $this->latitudeSource = $latitudeSource;

        return $this;
    }

    /**
     * Get latitudeSource
     *
     * @return string
     */
    public function getLatitudeSource()
    {
        return $this->latitudeSource;
    }

    /**
     * Set adresseLivraisonDestination
     *
     * @param string $adresseLivraisonDestination
     *
     * @return livraison
     */
    public function setAdresseLivraisonDestination($adresseLivraisonDestination)
    {
        $this->adresseLivraisonDestination = $adresseLivraisonDestination;

        return $this;
    }

    /**
     * Get adresseLivraisonDestination
     *
     * @return string
     */
    public function getAdresseLivraisonDestination()
    {
        return $this->adresseLivraisonDestination;
    }

    /**
     * Set longitudeDestination
     *
     * @param string $longitudeDestination
     *
     * @return livraison
     */
    public function setLongitudeDestination($longitudeDestination)
    {
        $this->longitudeDestination = $longitudeDestination;

        return $this;
    }

    /**
     * Get longitudeDestination
     *
     * @return string
     */
    public function getLongitudeDestination()
    {
        return $this->longitudeDestination;
    }

    /**
     * Set latitudeDestination
     *
     * @param string $latitudeDestination
     *
     * @return livraison
     */
    public function setLatitudeDestination($latitudeDestination)
    {
        $this->latitudeDestination = $latitudeDestination;

        return $this;
    }

    /**
     * Get latitudeDestination
     *
     * @return string
     */
    public function getLatitudeDestination()
    {
        return $this->latitudeDestination;
    }

    /**
     * Set datelivraison
     *
     * @param \DateTime $datelivraison
     *
     * @return livraison
     */
    public function setDatelivraison($datelivraison)
    {
        $this->datelivraison = $datelivraison;

        return $this;
    }

    /**
     * Get datelivraison
     *
     * @return \DateTime
     */
    public function getDatelivraison()
    {
        return $this->datelivraison;
    }

    /**
     * Set etat
     *
     * @param string $etat
     *
     * @return livraison
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string
     */
    public function getEtat()
    {
        return $this->etat;
    }

}

