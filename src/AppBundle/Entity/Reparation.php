<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reparation
 *
 * @ORM\Table(name="reparation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReparationRepository")
 */
class Reparation
{
    /**
     * @var int
     *
     * @ORM\Column(name="idReparation", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idReparation;

    /**
     * @var int
     *
     * @ORM\Column(name="idProduit", type="integer")
     */
    private $idProduit;

    /**
     * @var int
     *
     * @ORM\Column(name="idReparateur", type="integer",nullable=true)
     */
    private $idReparateur;

    /**
     * @return int
     */
    public function getIdReparateur()
    {
        return $this->idReparateur;
    }

    /**
     * @param int $idReparateur
     */
    public function setIdReparateur($idReparateur)
    {
        $this->idReparateur = $idReparateur;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="EtatReparation", type="string", length=255)
     */
    private $etatReparation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateReparation", type="date", nullable=true)
     */
    private $dateReparation;

    /**
     * @var float
     *
     * @ORM\Column(name="PrixReparation", type="float", nullable=true)
     */
    private $prixReparation;


    /**
     * Get id
     *
     * @return integer
     */
    public function getIdReparation()
    {
        return $this->idReparation;
    }

    /**
     * Set idProduit
     *
     * @param integer $idProduit
     *
     * @return Reparation
     */
    public function setIdProduit($idProduit)
    {
        $this->idProduit = $idProduit;
    
        return $this;
    }

    /**
     * Get idProduit
     *
     * @return integer
     */
    public function getIdProduit()
    {
        return $this->idProduit;
    }

    /**
     * Set etatReparation
     *
     * @param string $etatReparation
     *
     * @return Reparation
     */
    public function setEtatReparation($etatReparation)
    {
        $this->etatReparation = $etatReparation;
    
        return $this;
    }

    /**
     * Get etatReparation
     *
     * @return string
     */
    public function getEtatReparation()
    {
        return $this->etatReparation;
    }

    /**
     * Set dateReparation
     *
     * @param \DateTime $dateReparation
     *
     * @return Reparation
     */
    public function setDateReparation($dateReparation)
    {
        $this->dateReparation = $dateReparation;
    
        return $this;
    }

    /**
     * Get dateReparation
     *
     * @return \DateTime
     */
    public function getDateReparation()
    {
        return $this->dateReparation;
    }

    /**
     * Set prixReparation
     *
     * @param float $prixReparation
     *
     * @return Reparation
     */
    public function setPrixReparation($prixReparation)
    {
        $this->prixReparation = $prixReparation;
    
        return $this;
    }

    /**
     * Get prixReparation
     *
     * @return float
     */
    public function getPrixReparation()
    {
        return $this->prixReparation;
    }
}

