<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * produit
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\produitRepository")
 */
class produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="IdProduit", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idProduit;
    /**
     * @var int
     *
     * @ORM\Column(name="IdUser", type="integer")
     */
    private $idUser;

    /**
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param int $idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }
    /**
     * @var string
     *
     * @ORM\Column(name="NomProduit", type="string", length=255)
     */
    private $nomProduit;

    /**
     * @var string
     *
     * @ORM\Column(name="DescriptionProduit", type="string", length=255)
     */
    private $descriptionProduit;

    /**
     * @var string
     * @Assert\NotBlank(message="plz enter an image")
     * @Assert\Image()
     * @ORM\Column(name="ImageProduit", type="string", length=255)
     */
    private $imageProduit;

    /**
     * @var string
     *
     * @ORM\Column(name="OperationProduit", type="string", length=255)
     */
    private $operationProduit;

    /**
     * @var string
     *
     * @ORM\Column(name="CorbeilleProduit", type="string", length=255)
     */
    private $corbeilleProduit;

    /**
     * @return string
     */
    public function getCorbeilleProduit()
    {
        return $this->corbeilleProduit;
    }

    /**
     * @param string $corbeilleProduit
     */
    public function setCorbeilleProduit($corbeilleProduit)
    {
        $this->corbeilleProduit = $corbeilleProduit;
    }

    /**
     * Get idProduit
     *
     * @return int
     */
    public function getIdProduit()
    {
        return $this->idProduit;
    }

    /**
     * Set nomProduit
     *
     * @param string $nomProduit
     *
     * @return produit
     */
    public function setNomProduit($nomProduit)
    {
        $this->nomProduit = $nomProduit;

        return $this;
    }

    /**
     * Get nomProduit
     *
     * @return string
     */
    public function getNomProduit()
    {
        return $this->nomProduit;
    }

    /**
     * Set descriptionProduit
     *
     * @param string $descriptionProduit
     *
     * @return produit
     */
    public function setDescriptionProduit($descriptionProduit)
    {
        $this->descriptionProduit = $descriptionProduit;

        return $this;
    }

    /**
     * Get descriptionProduit
     *
     * @return string
     */
    public function getDescriptionProduit()
    {
        return $this->descriptionProduit;
    }

    /**
     * Set imageProduit
     *
     * @param string $imageProduit
     *
     * @return produit
     */
    public function setImageProduit($imageProduit)
    {
        $this->imageProduit = $imageProduit;

        return $this;
    }

    /**
     * Get imageProduit
     *
     * @return string
     */
    public function getImageProduit()
    {
        return $this->imageProduit;
    }

    /**
     * Set operationProduit
     *
     * @param string $operationProduit
     *
     * @return produit
     */
    public function setOperationProduit($operationProduit)
    {
        $this->operationProduit = $operationProduit;

        return $this;
    }

    /**
     * Get operationProduit
     *
     * @return string
     */
    public function getOperationProduit()
    {
        return $this->operationProduit;
    }
}

