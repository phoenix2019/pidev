<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Demande
 *
 * @ORM\Table(name="demande")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DemandeRepository")
 */
class Demande
{
    /**
     * @var int
     *
     * @ORM\Column(name="idDemande", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idDemande;

    /**
     * @var int
     *
     * @ORM\Column(name="idEchange", type="integer")
     */
    private $idEchange;

    /**
     * @var int
     *
     * @ORM\Column(name="idUser", type="integer")
     */
    private $idUser;

    /**
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param int $idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="etatDemande", type="string",length=255)
     */
    private $etatDemande;

    /**
     * @return string
     */
    public function getEtatDemande()
    {
        return $this->etatDemande;
    }

    /**
     * @param string $etatDemande
     */
    public function setEtatDemande($etatDemande)
    {
        $this->etatDemande = $etatDemande;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getIdDemande()
    {
        return $this->idDemande;
    }

    /**
     * Set idEchange
     *
     * @param integer $idEchange
     *
     * @return Demande
     */
    public function setIdEchange($idEchange)
    {
        $this->idEchange = $idEchange;
    
        return $this;
    }

    /**
     * Get idEchange
     *
     * @return integer
     */
    public function getIdEchange()
    {
        return $this->idEchange;
    }
}

