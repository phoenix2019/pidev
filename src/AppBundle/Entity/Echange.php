<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Echange
 *
 * @ORM\Table(name="echange")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EchangeRepository")
 */
class Echange
{
    /**
     * @var int
     *
     * @ORM\Column(name="idEchange", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idEchange;

    /**
     * @var int
     *
     * @ORM\Column(name="idUser1", type="integer")
     */
    private $idUser1;

    /**
     * @var int
     *
     * @ORM\Column(name="idUser2", type="integer", nullable=true)
     */
    private $idUser2;

    /**
     * @var int
     *
     * @ORM\Column(name="idProduit1", type="integer")
     */
    private $idProduit1;

    /**
     * @var int
     *
     * @ORM\Column(name="idProduit2", type="integer", nullable=true)
     */
    private $idProduit2;

    /**
     * @var string
     *
     * @ORM\Column(name="EtatEchange", type="string", length=255, nullable=true)
     */
    private $etatEchange;


    /**
     * Get id
     *
     * @return integer
     */
    public function getIdEchange()
    {
        return $this->idEchange;
    }

    /**
     * Set idUser1
     *
     * @param integer $idUser1
     *
     * @return Echange
     */
    public function setIdUser1($idUser1)
    {
        $this->idUser1 = $idUser1;
    
        return $this;
    }

    /**
     * Get idUser1
     *
     * @return integer
     */
    public function getIdUser1()
    {
        return $this->idUser1;
    }

    /**
     * Set idUser2
     *
     * @param integer $idUser2
     *
     * @return Echange
     */
    public function setIdUser2($idUser2)
    {
        $this->idUser2 = $idUser2;
    
        return $this;
    }

    /**
     * Get idUser2
     *
     * @return integer
     */
    public function getIdUser2()
    {
        return $this->idUser2;
    }

    /**
     * Set idProduit1
     *
     * @param integer $idProduit1
     *
     * @return Echange
     */
    public function setIdProduit1($idProduit1)
    {
        $this->idProduit1 = $idProduit1;
    
        return $this;
    }

    /**
     * Get idProduit1
     *
     * @return integer
     */
    public function getIdProduit1()
    {
        return $this->idProduit1;
    }

    /**
     * Set idProduit2
     *
     * @param integer $idProduit2
     *
     * @return Echange
     */
    public function setIdProduit2($idProduit2)
    {
        $this->idProduit2 = $idProduit2;
    
        return $this;
    }

    /**
     * Get idProduit2
     *
     * @return integer
     */
    public function getIdProduit2()
    {
        return $this->idProduit2;
    }

    /**
     * Set etatEchange
     *
     * @param string $etatEchange
     *
     * @return Echange
     */
    public function setEtatEchange($etatEchange)
    {
        $this->etatEchange = $etatEchange;
    
        return $this;
    }

    /**
     * Get etatEchange
     *
     * @return string
     */
    public function getEtatEchange()
    {
        return $this->etatEchange;
    }
}

