<?php

namespace AppBundle\EventListener;
use AppBundle\Entity\livraison;
use Doctrine\ORM\EntityManagerInterface;
use Toiba\FullCalendarBundle\Entity\Event;
use Toiba\FullCalendarBundle\Event\CalendarEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class FullCalendarListener
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    public function __construct(EntityManagerInterface $em, UrlGeneratorInterface $router)
    {
        $this->em = $em;
        $this->router = $router;
    }
    public function loadEvents(CalendarEvent $calendar)
    {
        $startDate = $calendar->getStart();
        $endDate = $calendar->getEnd();
        $filters = $calendar->getFilters();

        $livraison=$this->em->getRepository(livraison::class)->findAll();


        foreach($livraison as $l) {


            if( $l->getDatelivraison() != null ){
            // this create the events with your own entity (here booking entity) to populate calendar
            $bookingEvent = new Event(
                $l->getIdlivraison(),
                $l->getDatelivraison(),
                $l->getHeureLivraison() // If the end date is null or not defined, it creates a all day event
            );
            }
        }
            $calendar->addEvent($bookingEvent);

//        // You may want to make a custom query to populate the calendar
//
//        $calendar->addEvent(new Event(
//            'Event 1',
//            new \DateTime('Tuesday this week'),
//            new \DateTime('Wednesdays this week')
//        ));
//
//        // If the end date is null or not defined, it creates a all day event
//        $calendar->addEvent(new Event(
//            'Event All day',
//            new \DateTime('Friday this week')
//        ));



}
}